C = clang # replace this with gcc to run with gcc
SRC_DIR := .
OBJ_DIR := obj
OUT_DIR = bin
TARGET_STATIC = libnmlparser.a
TARGET_SHARED = libnmlparser.so

SRC_EXT = h
SRC_FILES := $(wildcard $(SRC_DIR)/*.$(SRC_EXT))
OBJ_FILES := $(patsubst $(SRC_DIR)/%,$(OBJ_DIR)/%,$(SRC_FILES:.$(SRC_EXT)=.o))
FLAGS := --std=c11 -Wall -Wextra -g

print-%: ; @echo $*=$($*)

run: static shared

static: $(OBJ_FILES)
	@mkdir -p $(OUT_DIR);
	ar -rc $(OUT_DIR)/$(TARGET_STATIC) $^

shared: $(SRC_FILES)
	@mkdir -p $(OUT_DIR);
	$(C) -fPIC -shared $^ -o $(OUT_DIR)/$(TARGET_SHARED)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.$(SRC_EXT)
	@mkdir -p $(OBJ_DIR);
	$(C) $(FLAGS) -c -o $@ $<

clean:
	@echo "Cleaning...";
	$(RM) -r $(OBJ_DIR) $(OUT_DIR)/$(TARGET_STATIC) $(OUT_DIR)/$(TARGET_SHARED)

.PHONY: clean
