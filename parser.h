#include <stdint.h>
#include <stdlib.h>

void nml_error(const char *message);

typedef struct hex_colours
{
    char red[2];
    char green[2];
    char blue[2];
} colour; // temp

/* COLOURS
struct rgb_colours
{
    uint8_t value[3];
};
struct rgb_colours init_colour(uint8_t red, uint8_t blue, uint8_t green)
{
    rgb_colours* rgb = malloc( sizeof( rgb_colours ) );
    rgb->value = { red, blue, green };

    return rgb;
}


typedef struct named_colours
{
    rgb_colours blue = init_colour( 0, 255, 0 );
    const char *black;
    const char *white;
} colour;
*/
typedef struct uiObjects
{
    int width;
    int height;
    colour textcolour;
    colour background;
    int x;
    int y;
    int z;
    const char *id;
    const char *type;
    const char *classes[];
} uiObject;

typedef struct imgObjects
{
    const char *source;
    struct uiObjects base;
} imgObject;

int parse( const char* name )
{
    if (name[0] == '\0' )
        nml_error("parser required a file");

    FILE *file;
    //char buffer[255];
    if ((file = fopen( name, "r")) == NULL)
    {
        nml_error("could not open file");
    }

    char c_char;
    while ((c_char = fgetc(file)) != EOF)
    {
        printf( "%c", c_char);
    }

    fclose( file );
    return 0;
}

void nml_error( const char* message )
{
    printf( "%s", message );
    exit(-1);
}